@extends('layouts.master')

@section('title')
    Conference Call
@endsection

@section('content')
    <h1>Conference Participants</h1>
     <hr />
    @if (sizeof($participants)>0)
     <ul class="conference-participants">
	  @foreach($participants as $participant)
		<li>
			 <h4><strong>Number: </strong> {{ $participant['number'] }}</h4>
			 <h4><strong>Conference Member Type: </strong> {{ $participant['memberType'] }}</strong></h4>
			 <h4><strong>Conference Member Status: </strong> {{ $participant['status'] }}</strong></h4>
		  
		</li>
	 @endforeach
	 </ul>
    @else
     <h2>No Active Conference Members</h2>
    @endif

@endsection('content')
