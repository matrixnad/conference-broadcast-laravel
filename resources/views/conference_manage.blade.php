@extends('layouts.master')

@section('title')
    Conference Call
@endsection

@section('content')
   <h1>Manage the conference settings</h1>
   <hr />

   <form method="POST" action="/conference/manage">
    <h1 class="text-center">Conference Line: 
    <input type='text' style='background:#3D3D3D; ' name='conferenceNumber' value='{{ $conferenceNumber }}'></input>
    </h1>
    <h1 class="text-center">Conference Name:
    <input type='text' style='background:#3D3D3D; ' name='conferenceName' value='{{ $conferenceName }}'></input>
    </h1>
    <input type='hidden' name='_method' value='POST'></input>
    <input type='hidden' name='_token' value='{{ csrf_token() }}'></input>
     <br />
    <input type="submit" class="btn btn-large btn-primary btn-default" value="Save Changes"></input>
  </form>
   <hr />
@endsection('content')
