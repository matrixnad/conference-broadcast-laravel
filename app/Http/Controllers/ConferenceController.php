<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Services_Twilio_Twiml;
use Services_Twilio;

class ConferenceController extends Controller
{
    public function addParticipant($data) { // number and user type
	  $db=$this->getDatabase();
  	  //check for existance
	  $result = $db->query(sprintf("SELECT * FROM `conferenceParticipants` WHERE `number` ='%s'", $data['number']), \PDO::FETCH_ASSOC);
	 $record=$this->getSingleRecord($result);
	  if ($record) { //update the status
		$db->exec(sprintf("UPDATE `conferenceParticipants` SET `status` ='ONLINE', `memberType` ='%s' WHERE `number`='%s'", $data['number'], 
			$data['type']
			));
	   } else {
		// add  a new user
		$db->exec(sprintf("INSERT INTO `conferenceParticipants` (number,status,memberType) VALUES ('%s', '%s', '%s')", 
			$data['number'], "ONLINE", $data['type']));
		}

    }
    public function index()
    {
	$config=$this->getConfig();
        $conferenceNumber = $config->conferenceNumber;
        return view('conference', ['conferenceNumber' => $conferenceNumber]);
    }
    public function participantsConference()  
    {
		$participants=$this->getConferenceParticipantsV2();
	   	$config=$this->getConfig();
		return view('conference_participants', ['conferenceNumber' => $config->conferenceNumber,
			'participants' => $participants
			]);
    }
    public function manageConference(Request $request)
    {
	 	$method =$request->method();
		if ( $method == "POST" ) {
			$newConferenceName=$request->conferenceName;
		  	 $newConferenceNumber = $request->conferenceNumber;
		  	 $this->updateConferenceName($newConferenceName);
		  	 $this->updateConferenceNumber($newConferenceNumber);
			return view('conference_manage', ['conferenceName' => $newConferenceName, 'conferenceNumber' => $newConferenceNumber]);
	 	} else {
			$config = $this->getConfig();
			return view('conference_manage', ['conferenceName' =>$config->conferenceName,
				'conferenceNumber' => $config->conferenceNumber
				]);
		}
    }


    /**
     * Replies with a join call xml
     *
     * @return \Illuminate\Http\Response
     */
    public function showJoin(Request $request)
    {
        $response = new Services_Twilio_Twiml;
	$config = $this->getConfig();
        $response->say(
            sprintf('You are about to join the %s conference.', $config->conferenceName),
            ['voice' => 'alice', 'language' => 'en-GB']
        );
	$routeRequested = route('conference-connect',[],false)."?From=".$request->From;
        $gather = $response->gather(
            ['numDigits' => 1,
             'action' => $routeRequested,
             'method' => 'GET'
            ]
        );
        $gather->say('Press 1 to join as a listener.');
        $gather->say('Press 2 to join as a speaker.');
        $gather->say('Press 3 to join as the moderator.');

        return response($response)->header('Content-Type', 'application/xml');
    }
    public function getUserType($digitGiven) 
    {
	  if ( (int)$digitGiven == 1 ) {
		return "LISTENER";
	  } else if ((int)$digitGiven == 2) {
		 return "SPEAKER";
	  }  else if ((int)$digitGiven == 3 ) {
		return "MODERATOR";
	 }
    }
    public function getSingleRecord($queryResult) 
    {
       $result=null;
	 if ($queryResult) {
		foreach ($queryResult->fetchAll() as $queryResult1 ) {
			return $queryResult1;
		  }
	 }
	 return $result;
    }
    public function getRecords($queryResult)
    {
	 $results=[];
	 if ($queryResult) {
		 foreach($queryResult->fetchAll() as  $result) {
			$results[] = $result;
			}
		}
	 return $results;
		
    }
    public function getDatabase()
    {
  	 $sqlite = new  \PDO("sqlite:".(__DIR__ . "/../../../db.sql"));
	 $sqlite->setAttribute( \PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);

	 $sqlite->exec( "
			CREATE TABLE IF NOT EXISTS `conferenceParticipants` (
				`number` varchar(255),
				`memberType` varchar(255),
				`status` varchar(255)
			);
	  ");
		
	 return $sqlite;
	}
    public function getConfig()
    {
	  return json_decode(file_get_contents(__DIR__."/../../../config.json"));
    }
    public function getClient()
    {
        $config=$this->getConfig();
	$client = new Services_Twilio($config->appSid,$config->appSecret);
	return $client;
    }
    public function getConference()
    {
	 $config=$this->getConfig();
	 $client = $this->getClient();
	 $conferences=$client->account->conferences->getIterator();
	 foreach ($conferences as $conference) {
		 if ($conference->friendly_name==$config->conferenceName) {
				return $conference;
		 }
	  }
	 return null;
	}


    public function getConferenceParticipantsV2() 
    {
	  $database=$this->getDatabase();
	  $query = $database->query(" SELECT * FROM `conferenceParticipants` ", \PDO::FETCH_ASSOC);
	  $results=$this->getRecords($query  );
	 return $results;
    }


    public function getConferenceParticipants($conference) { // by call
	 $participants=$conference->participants;
	 $byCallData=[];
	 $client=$this->getClient();
	 foreach($participants as $participant) {
		$call = $client->account->calls->get($participant->call_sid);
		$byCallData[]=[
			'number' => $call->from
		];
	   }
 	 return $byCallData;
     }
   public function updateConferenceName($newName) {
	 $config=$this->getConfig();
	 $config->conferenceName=$newName;
     	 $this->updateConfig($config);
   }
   public function updateConferenceNumber($newNumber) {
	$config=$this->getConfig();
	$config->conferenceNumber=$newNumber;
	$this->updateConfig($config);
	}
   public function updateConfig($newConfig){
	 file_put_contents(__DIR__."/../../../config.json", json_encode($newConfig));
     }
			
		

    /**
     * Replies with a call connect xml
     *
     * @return \Illuminate\Http\Response
     */
    public function showConnect(Request $request)
    {
        $muted = false;
        $moderator = false;
        $digits = $request->input('Digits');
        if ($digits === '1') {
            $muted = true;
        }
        if ($digits === '3') {
            $moderator = true;
        }

	$config= $this->getConfig();
        $response = new Services_Twilio_Twiml;
        $response->say(
            'You have joined the conference.',
            ['voice' => 'alice', 'language' => 'en-GB']
        );
        $dial = $response->dial();
        $dial->conference($config->conferenceName, [
            'startConferenceOnEnter' => $moderator,
            'endConferenceOnExit' => $moderator,
            'muted' => $muted,
            'waitUrl' => 'http://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient',
        ]);
	$userType =  $this->getUserType( $digits );
	$this->addParticipant( [ 
		'number' => $request->input('From'),
		'type' => $userType
	]);

        return response($response)->header('Content-Type', 'application/xml');
    }
   public function showEnd(Request $request)
   {
      $db =$this->getDatabase();
      $query=sprintf("DELETE FROM `conferenceParticipants` WHERE `number` ='%s'",$request->input('From')) ;
	$db->exec( $query );
    }

}
